% Volume Integral formulation using expressions from Richmond's paper
% https://ieeexplore.ieee.org/abstract/document/1138427
% The code computes the scattered field from a rectangular object
% Author - D.V.S.S.N.Karteekeya Sastry, IIT Madras  Dated: 28/3/18
% exp(jwt) convention
clear all;
close all;
clc;

%%
%1. Set parameters of simulation
lambda = 1;
k0 = 2*pi/lambda;
inc = 0; %angle of incidence
%%
%2.Generate the object of simulation -- rectangle
eps = 2.0; %relative permittivity
%k1 = sqrt(eps)*k0; 
obs = 3 * lambda; %observation radius
m=8; %Number of edges along breadth
l=1*lambda;
b=0.5*lambda;
le=b/m;
x=le/2:le:l-le/2; %x values of midpoints
y=le/2:le:b-le/2; %y values of midpoints
x=x-(l/2);  y=y-(b/2); %Shifting origin to center of object
mid=zeros(2*m^2,2); %array to store midpoints of cells 
n=size(mid,1);
for i=1:2*m
    for j=1:m
        mid((i-1)*m+j,:)=[x(i) y(j)];
    end
end
a=le/sqrt(pi); %equivalent circle radius
%%
%3. Create the matrix for computing unknown currents
A=zeros(n); %Symmetric matrix for given problem 
b=zeros(n,1);
rho=0;
incfn = @(p,inc,k) exp(-1j*k*(p(1)*cos(inc)+p(2)*sin(inc)));
%set LHS matrix A
for i=1:n
    for j=1:n
        if i==j
            A(i,i) = 1+((eps-1)*(1i/2)*(pi*k0*a*besselh(1,2,k0*a)-2*1i));
            %set RHS vector b
            b(i) = incfn(mid(i,:),inc,k0);
            break;
        else
            rho=sqrt((mid(i,1)-mid(j,1))^2+(mid(i,2)-mid(j,2))^2);
            A(i,j)=((1i*pi*k0*a)/2)*(eps-1)*besselj(1,k0*a)*besselh(0,2,k0*rho);
            A(j,i)=A(i,j);
        end
    end
end

%%
%4. Solve the matrix
x=A\b;

%%
%5. Compute far field
obs_n = 300; obs_t = 2*pi/obs_n;
rxth = 0:obs_t:2*pi-obs_t/2;
ff = zeros(obs_n,1); % Variable to store far field
for i=1:length(rxth) %observation points
    p = [obs*cos(rxth(i)-obs_t/2.0) obs*sin(rxth(i)-obs_t/2.0)]; %start of ith seg
    for j=1:n 
        rho=sqrt((p(1)-mid(j,1))^2+(p(2)-mid(j,2))^2);
        ff(i)=ff(i)-((1i*pi*k0/2)*(eps-1)*x(j)*a*besselj(1,k0*a)*besselh(0,2,k0*rho));
    end
end

%%
% 6. Plotting
figure;
polar(rxth',abs(ff));
title('Absolute value of the scattered field for volume integral at 3\lambda');
saveas(gcf,['m=' num2str(m) ' magnitude' '.png']);