## Welcome to the code repository for computational electromagnetics from Uday Khankhoje's group at IIT Madras
## This contains a high level description of the codes available in this repository

1. A surface integral formulation in the TM polarization in two dimensions written in MATLAB. The code computes the scattering from a homogeneous cylinder using the Method of Moments and a pulse basis function, delta testing function approach. The file starting with "main" is the main file, and the other two files compute the Green's function and the gradient of the Green's function. Navigate to the "Surf_Intg_Eqn_TM_2D" folder.

2. A volume integral formulation in the TM polarization in two dimensions written in MATLAB. The code computes the scattering from a homogeneous cylinder using the Method of Moments and a pulse basis function, delta testing function approach. There is only one file for this code in the "Vol_Intg_Eqn_TM_2D" folder.
