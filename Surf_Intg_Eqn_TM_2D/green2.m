function [ y ] = green2(k,p0x,p0y,s,pex,pey,r0x,r0y,t,rex,rey)
%Green's fn y = j/4 H_0^(2)(k|(p+s*p_ed)-(r+t*r_ed))
%s from 0 to 1 fixes p, t from 0 to 1 fixes r
nm = @(x,y) sqrt(x.^2+y.^2);
dx = (r0x+t.*rex)-(p0x+s.*pex);
dy = (r0y+t.*rey)-(p0y+s.*pey);
y = 1j/4 * besselh(0,2,k.*nm(dx,dy));
end