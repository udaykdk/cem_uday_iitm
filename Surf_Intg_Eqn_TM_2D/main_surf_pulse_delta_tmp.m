%Script to test out cylinder scattering using field formulation
%of surface EFIE, Uday Khankhoje 10 Feb 2018. exp(jwt) conv
%clear
%close all
tic
%1. Set parameters of simulation
lambda = 1;
k0 = 2*pi/lambda;
inc = 0; %angle of incidence
tolabs = 1e-9; %absolute tolerance in integral
tolrel = 1e-6; %relative tolerance in integral

%2.Generate the object of simulation -- cylinder
a = 1 * lambda; %cyl radius
n = 90; %discretization parameter: number of segments along the perimeter
eps = 2.0; %relative permittivity
th = 2*pi/n;
oth = 0:th:(2*pi-th/2); %these are the testing centres
obs = 2 * lambda; %observation radius
w = a * sin(th/2);

%3. Create the matrix for computing unknown currents
% The two integrals are: 
%oint[g1(r,p) grad(phi).n - grad(g1(r,p).n phi]dr = phi_inc(p)  (1)
%oint[g2(r,p) grad(phi).n - grad(g2(r,p).n phi]dr = 0           (2)
A = zeros(2*n,2*n); b = zeros(2*n,1);
%be sure to check sign convenctions for Green's functions below
%use greens fn = j/4 H_0^(2)(k|r-r'|) and 
%gradgreensfn = -jk/4 H_1^(2)(k|r-r'|)hat(r-r')
%We can use these inline functions (below) or define them separately
%greenfn = @(t,p,r,r_ed,k) j/4*besselh(0,2,k*norm(p-(r+t.*r_ed)));
%gradgreenfn = @(t,p,r,r_ed,k,n) -1j*k/4*besselh(1,2,k*norm(p-(r+t.*(r_ed))))*dot(r_ed,n)/norm(r_ed);
incfn = @(p,inc,k) exp(-1j*k*(p(1)*cos(inc)+p(2)*sin(inc)));
%set LHS matrix A
for i=1:n %row no -- p (for primed) coordinate
    p = [a*cos(oth(i)-th/2.0) a*sin(oth(i)-th/2.0)]; %start of ith seg
    p_tmp = [a*cos(oth(i)+th/2.0) a*sin(oth(i)+th/2.0)];
    p_ed = p_tmp - p;
    for j=1:n %col no -- r coordinate
		r = [a*cos(oth(j)-th/2.0) a*sin(oth(j)-th/2.0)]; %start of jth segment
        r_tmp = [a*cos(oth(j)+th/2.0) a*sin(oth(j)+th/2.0)]; 
        r_ed = r_tmp - r;
		nhat = [cos(oth(j)) sin(oth(j))];         
        if i==j %in this case the integrals are symmetric
            %for eqn (1)
            A(i,j) = 2.0*integral(@(t)green2(k0,p(1),p(2),0.5,p_ed(1),p_ed(2),r(1),r(2),t,r_ed(1),r_ed(2)),0.5,1,'AbsTol',tolabs,'RelTol',tolrel);
            %for eqn (2)tolabs
            A(i+n,j) = 2.0*integral(@(t)green2(eps*k0,p(1),p(2),0.5,p_ed(1),p_ed(2),r(1),r(2),t,r_ed(1),r_ed(2)),0.5,1,'AbsTol',tolabs,'RelTol',tolrel);
        else %in this case the integrals are not symmetric
            %for eqn (1)
            A(i,j) = integral(@(t)green2(k0,p(1),p(2),0.5,p_ed(1),p_ed(2),r(1),r(2),t,r_ed(1),r_ed(2)),0.0,1,'AbsTol',tolabs,'RelTol',tolrel);
            A(i,j+n) = -1.0 * integral(@(t)gradgreen2(k0,p(1),p(2),0.5,p_ed(1),p_ed(2),r(1),r(2),t,r_ed(1),r_ed(2),nhat(1),nhat(2)),0.0,1,'AbsTol',tolabs,'RelTol',tolrel); 
            %for eqn (2)
            A(i+n,j) = integral(@(t)green2(eps*k0,p(1),p(2),0.5,p_ed(1),p_ed(2),r(1),r(2),t,r_ed(1),r_ed(2)),0.0,1,'AbsTol',tolabs,'RelTol',tolrel);
            A(i+n,j+n) = -1.0 * integral(@(t)gradgreen2(eps*k0,p(1),p(2),0.5,p_ed(1),p_ed(2),r(1),r(2),t,r_ed(1),r_ed(2),nhat(1),nhat(2)),0.0,1,'AbsTol',tolabs,'RelTol',tolrel); 
        end
    end
    %set RHS vector b
    b(i) = incfn(p+0.5*p_ed,inc,k0);
end
A = 2.0 * w * A;

%4. Solve the matrix
x = A\b;

%5. Compute far field and output
% phi(p) = phi_inc(p) - oint[g1(r,p) grad(phi).n - grad(g1(r,p).n phi]dr - (3)
obs_n = 180; obs_t = 2*pi/obs_n;
rxth = 0:obs_t:2*pi-obs_t/2;
ff = zeros(obs_n,1);
for i=1:length(rxth) %observation points
    p = [obs*cos(rxth(i)-obs_t/2.0) obs*sin(rxth(i)-obs_t/2.0)]; %start of ith seg
    p_tmp = [obs*cos(rxth(i)+obs_t/2.0) obs*sin(rxth(i)+obs_t/2.0)];
    p_ed = p_tmp - p;
    for j=1:n %segments of the contour integral
        r = [a*cos(oth(j)-th/2.0) a*sin(oth(j)-th/2.0)]; %start of jth segment
        r_tmp = [a*cos(oth(j)+th/2.0) a*sin(oth(j)+th/2.0)]; 
        r_ed = r_tmp - r;
		nhat = [cos(oth(j)) sin(oth(j))];
        ff(i) =  ff(i) - 2 * w *(x(j)*integral(@(t)green2(k0,p(1),p(2),0.5,p_ed(1),p_ed(2),r(1),r(2),t,r_ed(1),r_ed(2)),0.0,1,'AbsTol',tolabs,'RelTol',tolrel) ...
        - x(j+n)*integral(@(t)gradgreen2(k0,p(1),p(2),0.5,p_ed(1),p_ed(2),r(1),r(2),t,r_ed(1),r_ed(2),nhat(1),nhat(2)),0.0,1,'AbsTol',tolabs,'RelTol',tolrel));
    end
    ff(i) = ff(i) + 0.0*incfn(p+0.5*p_ed,inc,k0); %scattered (0), or total (1) field
end
figure()
subplot(1,1,1); polarplot(rxth,abs(ff)); title(['n=' num2str(n)]); saveas(gcf,['n=' num2str(n) '.png']);
%subplot(1,2,2); polarplot(oth,abs(x(1:n)));
toc