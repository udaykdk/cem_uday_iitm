function [ y ] = gradgreen2(k,p0x,p0y,s,pex,pey,r0x,r0y,t,rex,rey,nx,ny)
%Grad(greenfn).hat(n) y = -jk/4 H_1^(2)(k|r-r'|)hat(r-r') dot hat(n)
%s from 0 to 1 fixes p, t from 0 to 1 fixes r
nm = @(x,y) sqrt(x.^2+y.^2);
dx = (r0x+t.*rex)-(p0x+s.*pex);
dy = (r0y+t.*rey)-(p0y+s.*pey);
dotprod = nx .* dx + ny .* dy;
if abs(dotprod)< 1e-8
    y = 0;
else
    y = -1j*k/4*besselh(1,2,k.*nm(dx,dy)) .* dotprod./nm(dx,dy);
end
end
